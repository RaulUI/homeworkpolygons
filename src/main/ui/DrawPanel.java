package main.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JPanel;

import main.utils.AppHelper;
import main.utils.AppHelper.Mode;
import main.utils.AppHelper.Operation;
import main.utils.MyPoly;

public class DrawPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
    private static ArrayList<MyPoly> polygons;
    private static MyPoly selectedPolygon;
    private static Point clickLocation;
	private static Point[] tmpPoints;
	private static ArrayList<Color> polygonsFill;

    private int pointsSize = 8;
    private ArrayList<ArrayList<Point>> storedPoints;
    public static ArrayList<Point> points;
    public static boolean clearPoints = false, showPopup = false, mouseWheelMoved = false, scaleOut = true;
    
    private MouseAdapter mouseListener = new MouseAdapter() {
    	
    	// GET user mode and mouse button type clicked
    	// Act according to selection
        @Override
        public void mousePressed(MouseEvent e) {
        	
        	//DRAW Mode
        	if (e.getButton() == 1 && AppHelper.currentMode == Mode.Draw) {
        		points.add(new Point(e.getX(), e.getY()));
//        		storedPoints.get(storedPointsIndex).add(new Point(e.getX(), e.getY()));
        		pointsSize = 8;
        	} else if (AppHelper.currentMode == Mode.Draw) {
        		if (points.size() < 3)
        			showPopup = true;
        		else 
            		clearPoints = true;
        	// EDIT Mode
        	} else if (e.getButton() == 1 && AppHelper.currentMode == Mode.Edit && checkIfPointInPolygons(e)) {
        		clickLocation = e.getPoint();
    			selectedPolygon = getClickedPolygon(e);
        	} else if (AppHelper.currentMode == Mode.Edit && checkIfPointInPolygons(e)) {
        		clickLocation = e.getPoint();
        		selectedPolygon = getClickedPolygon(e);
        	} else if (e.getButton() == 1 && AppHelper.currentMode == Mode.Edit && !checkIfPointInPolygons(e)) {
        		selectedPolygon = new MyPoly();
        		AppHelper.currentColor = Color.DARK_GRAY;
        		if (AppHelper.currentOperation != Operation.Translate && AppHelper.currentOperation != Operation.Scale)
                	AppHelper.currentOperation = null;
        	} 
    		mouseWheelMoved = false;
    		repaint();
        }
        
        // Mouse dragging translating polygon
        @Override
        public void mouseDragged(java.awt.event.MouseEvent e) {
        	if (AppHelper.currentMode == Mode.Edit && AppHelper.currentOperation == Operation.Translate) {
        		for (Polygon poly : intersectsPolygon(selectedPolygon, polygons)) {
        			poly.translate(e.getX() - clickLocation.x, e.getY() - clickLocation.y);
        		}
	        	clickLocation = e.getPoint();
	            repaint();
        	}
        }
        
        // Mouse scrolled scaling selected polygon;
        @Override
        public void mouseWheelMoved(java.awt.event.MouseWheelEvent e) {
        	if (AppHelper.currentMode == Mode.Edit && AppHelper.currentOperation == Operation.Scale) {
        		
        		//Check if user rolled mouse wheel up or down
	        	if (e.getWheelRotation() < 0) {
	        		scaleOut = true;
	        	} else {
	        		scaleOut = false;
	    		}
        		mouseWheelMoved = true;
	    		repaint();
        	}
        }
    };

    public DrawPanel() {
    	
    	// Initializing variables
    	storedPoints = new ArrayList<ArrayList<Point>>();
    	storedPoints.add(new ArrayList<Point>());
        points = new ArrayList<Point>();
        polygons = new ArrayList<MyPoly>();
        clickLocation = new Point();
        selectedPolygon = new MyPoly();
        polygonsFill = new ArrayList<Color>();
        setBackground(Color.WHITE);
        
        // Adding mouse listeners
        addMouseListener(mouseListener);
        addMouseMotionListener(mouseListener);
        addMouseWheelListener(mouseListener);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // Painting the points
        drawPoints(g);
        
        //Painting the lines
    	drawLines(g);
    	
    	//Reseting operation
        if (AppHelper.currentOperation != Operation.Translate && AppHelper.currentOperation != Operation.Scale)
        	AppHelper.currentOperation = null;
        mouseWheelMoved = false;
    }
    
    public void drawLines(Graphics g) {
    	
    	// Reconstructing points matrix each time to update if anything changed
    	try {
    		setCurrentPoints ();
			tmpPoints = getOriginalPoints(polygons.indexOf(selectedPolygon));
    	} catch (Exception e) {}
    	
    	ArrayList<Polygon> polyToDelete = new ArrayList<Polygon>();

    	Graphics2D g2 = (Graphics2D)g;
    	
    	// Adding polygon to list with specific points for it
    	if (clearPoints && points.size() > 2) {
    		
	    	// Get each time the specific points for the current drawn polygon
	    	int [] pointsArrayX = new int[points.size()];
	    	int [] pointsArrayY = new int[points.size()];
	    	
	    	for (int i = 0; i < points.size(); i++) {
	    		pointsArrayX[i] = points.get(i).x;
	    		pointsArrayY[i] = points.get(i).y;
	    	}
	    	
    		polygons.add(new MyPoly(pointsArrayX, pointsArrayY, points.size(), null));
    		polygonsFill.add(null);
    	}
    	
    	for (MyPoly poly : polygons) {
    		
    		// Selected polygon
    		if (poly.equals(selectedPolygon)) {
    			
    			// Fill operation for selected polygon
    			if (AppHelper.currentOperation == Operation.Fill) {
    				
    				polygonsFill.set(polygons.indexOf(selectedPolygon), AppHelper.currentColor);
    				poly.setFillColor(AppHelper.currentColor);
    			}
    			
    			// Rotate operation for selected polygon
    			if (AppHelper.currentOperation == Operation.Rotate) {
    				
    				rotatePointMatrix(getOriginalPoints(polygons.indexOf(poly)), poly, tmpPoints);
    			}
    			
    			// Scale operation for selected polygon
    			if (mouseWheelMoved) {
    				scalePointsMatrix(getOriginalPoints(polygons.indexOf(poly)), poly, tmpPoints);
    			}
    			
    			// Create a new polygon for Rotate and Scale operations and swap it with the old one
            	// After swapping remove the old polygon from the list and canvas
            	try {
            		if (AppHelper.currentOperation == Operation.Rotate || (AppHelper.currentOperation == Operation.Scale && mouseWheelMoved)) {
            			
            			int index = polygons.indexOf(selectedPolygon);
            			Color tmpCol = polygons.get(index).getFillColor();
        	    		polygons.add(index, polygonize(tmpPoints));
        	    		polygons.get(index).setFillColor(tmpCol);
        	    		polygons.remove(selectedPolygon);
        	    		selectedPolygon = polygons.get(index);
        				mouseWheelMoved = false;

            		}
            	} catch (Exception e) {}
            	
    			g2.setColor(new Color(4, 209, 220));
    			g2.setStroke(new BasicStroke(3));
    		// Polygon is intersected by the selected polygon
			} else if (intersectsPolygon(selectedPolygon, polygons).contains(poly)) {
				
				// Fill operation for polygons which intersects with the selected polygon
//				if (AppHelper.currentOperation == Operation.Fill) {
//					
//					poly.setFillColor(selectedPolygon.getFillColor());
//    			}
				
				g2.setColor(new Color(3, 166, 148));
				g2.setStroke(new BasicStroke(1));
			
			// Other polygons
			} else {

				g2.setColor(Color.DARK_GRAY);
				g2.setStroke(new BasicStroke(1));
			}
    		
    		// Executes only if polygon was filled
    		try {
    			poly.getFillColor().equals(null);
    			g2.setColor(poly.getFillColor());
				g2.fill(poly);
				
				// Color the margin of selected polygon if it was filled and uncolor it otherwise
				if (poly.equals(selectedPolygon)) {
	    			g2.setColor(new Color(4, 209, 220));
	    			g2.setStroke(new BasicStroke(3));
				} else {
					g2.setColor(new Color(3, 166, 148));
					g2.setStroke(new BasicStroke(1));
				}
    		} catch (Exception e) {}

    		// Polygon is intersected by the selected polygon or it is the selected polygon and the operation is delete
    		if (AppHelper.currentOperation == Operation.Delete && (poly.equals(selectedPolygon) || intersectsPolygon(selectedPolygon, polygons).contains(poly))) {
    			
    			polyToDelete.add(poly);
			} else {

				g2.drawPolygon(poly);
			}
    	}
    	
    	// Clear points and add a new level to the stored points matrix
    	if (clearPoints) {
    		if (points.size() > 2) {
    			storedPoints.add(new ArrayList<Point>());
    		}
    		points.clear();
    		clearPoints = false;
    	}
    	
    	// If Delete operation was selected 'polyToDelete' list would contain
    	// elements, otherwise this won't do anything
    	polygons.removeAll(polyToDelete);
    }

	private void drawPoints(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		
		for (Point point : points) {
			g2.fillOval(point.x - pointsSize/2, point.y - pointsSize/2, pointsSize, pointsSize);
        }
	}
    
	/**
     * 
     * @param  e the MouseEvent which contains the position of clicked point
     * @return true if mouse pointed at any polygon from {@link DrawPanel#polygons}, false otherwise
     */
    private boolean checkIfPointInPolygons(MouseEvent e) {
    	for (Polygon poly : polygons) {
    		if (poly.contains(new Point(e.getX(), e.getY())))
    			return true;
    	}
    	return false;
    }
    
    /**
     * 
     * @param  e the MouseEvent which contains the position of clicked point
     * @return the polygon from {@link DrawPanel#polygons} where the mouse position is
     */
    private MyPoly getClickedPolygon(MouseEvent e) {
    	for (MyPoly poly : polygons) {
    		if (poly.contains(e.getPoint()))
    			return poly;
    	}
    	return null;
    }
    
    /**
     * 
     * @param areaPoly1 the area to be checked
     * @param poly2     the polygon in which we search for the area
     * @return          true if poly2 contains area, false otherwise
     */
    private static boolean intersectsPolygon(Area areaPoly1, Polygon poly2) {
    	Area areaPoly2 = new Area(poly2);
    	
    	areaPoly1.intersect(areaPoly2);
    	return !areaPoly1.isEmpty();
    }
    
    /**
     * 
     * @param  poly1 polygon for which we search intersection
     * @param  polys a list of polygons in which we search any intersections
     * @return a list with all the polygons with whom poly1 is intersecting from list polys    
     */
    private static ArrayList<MyPoly> intersectsPolygon(MyPoly poly1, ArrayList<MyPoly> polys) {
    	Area areaPoly1 = new Area(poly1);
    	ArrayList<MyPoly> intersectionGroup = new ArrayList<MyPoly>();
    	@SuppressWarnings("unchecked")
		ArrayList<MyPoly> tmpPolys = (ArrayList<MyPoly>) polys.clone();
    	
    	try {
    		Collections.swap(tmpPolys, 0, tmpPolys.indexOf(poly1));
    	} catch(Exception e) {}
    	
    	for (int i = 0; i < tmpPolys.size(); i++)
	    	for (MyPoly p : tmpPolys) {
	    		Area tmp = (Area) areaPoly1.clone();
	    		if(intersectsPolygon(tmp, p) || poly1.equals(p)) {
	    			areaPoly1.add(new Area(p));
	    			if (!intersectionGroup.contains(p))
	    				intersectionGroup.add(p);
	    		}
	    	}
    	
    	return intersectionGroup;
    }
    
    /**
     * 
     * @param  points an array of points
     * @return the center of the polygon formed from points
     */
	private Point polygonCenter (Point [] points) {
		int x = 0, y = 0;
		for (int i = 0; i < points.length; i++) {
			x += points[i].x;
			y += points[i].y;
		}
		return new Point (x / points.length, y / points.length);
	}
	
	/**
	 * 
	 * @param  polyIndex the index of the polygon from the polygons list
	 * @return a list of points from {@link DrawPanel#storedPoints}
	 */
	private Point [] getOriginalPoints(int polyIndex) {
//		return Arrays.stream(storedPoints.get(polyIndex))
//                .filter(p -> p != null)
//                .toArray(Point[]::new);
		Point[] tmpPoints = new Point[storedPoints.get(polyIndex).size()];
		storedPoints.get(polyIndex).toArray(tmpPoints);
		return tmpPoints;
	}
	
	/**
	 * Updating the current {@link DrawPanel#storedPoints} matrix with {@link DrawPanel#polygons} list
	 */
	private void setCurrentPoints () {
		storedPoints = new ArrayList<ArrayList<Point>> ();
		for (MyPoly poly : polygons) {
//			getPoints(poly).toArray(storedPoints[index]);
			storedPoints.add(getPoints(poly));
		}
	}
	
	/**
	 * 
	 * @param origPoints an array with the original points of the selected polygon
	 * @param poly       the selected polygon for which we calculate 
	 * 					 the center for the sizing method
	 * @param storeTo    an array to save the new polygon into
	 */
	private void scalePointsMatrix (Point[] origPoints, MyPoly poly, Point[] storeTo) {
		if (!mouseWheelMoved) 
			return;

		int scaleMultipier = 2;
		
		//Check if it is too small or too big and stop the user scaling in if it is
		// || (polygonArea(storeTo) > 75000 && scaleOut) add this to if for scale out limit 
		// with value '75000' with prefered polygon area to cap at
		if ((polygonArea(storeTo) < 10000 && !scaleOut))
			return;
		
		Point center = polygonCenter(origPoints);
		ArrayList<Point> points = new ArrayList<Point>();
		int index = 0;	
		
		
		for (Point point : origPoints) {
			
			points.add(new Point());
			
			//Check if user scaled out or in
			if (scaleOut) {
				points.get(index).x = point.x + (point.x - center.x) / scaleMultipier;
				points.get(index).y = point.y + (point.y - center.y) / scaleMultipier;
			} else {
				points.get(index).x = point.x - (point.x - center.x) / (scaleMultipier * 2);
				points.get(index).y = point.y - (point.y - center.y) / (scaleMultipier * 2);
			}
			index++;
		}

		points.toArray(storeTo);
	}
	
    private void rotatePointMatrix (Point[] origPoints, MyPoly poly, Point[] storeTo) {

        /* We get the original points of the polygon we wish to rotate
         *  and rotate them with affine transform to the given angle. 
         *  After the operation is complete the points are stored to the 
         *  array given to the method.
        */
        AffineTransform.getRotateInstance
        (Math.toRadians(90), polygonCenter(origPoints).x, polygonCenter(origPoints).y)
                .transform(origPoints, 0, storeTo, 0, origPoints.length);

    }
    
    /**
     * 
     * @param  poly the polygon to get the points from
     * @return a list with all the points from poly
     */
    public ArrayList<Point> getPoints (MyPoly poly) {
    	ArrayList<Point> points = new ArrayList<Point>();
    	for (int i = 0; i < poly.npoints; i++) {
    		points.add(new Point(poly.xpoints[i], poly.ypoints[i]));
    	}
    	return points;
    }
    
    /**
     * 
     * @param polyPoints an array of points to create a new polygon with
     * @return           a new polygon created from polyPoints
     */
    private MyPoly polygonize (Point[] polyPoints) {

        //a simple method that makes a new polygon out of the rotated points
    	MyPoly tempPoly = new MyPoly();

         for(int  i=0; i < polyPoints.length; i++){
             tempPoly.addPoint(polyPoints[i].x, polyPoints[i].y);
        }

        return tempPoly;
    }
    
    public static void setFillForIntersect() {
    	for (MyPoly poly : intersectsPolygon(selectedPolygon, polygons)) {
    		poly.setFillColor(AppHelper.currentColor);
    	}
    }
    
    private int polygonArea(Point [] points) 
    { 
      int area = 0;         // Accumulates area in the loop
      int j = points.length-1;  // The last vertex is the 'previous' one to the first

      for (int i = 0; i < points.length; i++)
        { area = area +  (points[j].x + points[i].x) * (points[j].y - points[i].y); 
          j = i;  //j is previous vertex to i
        }
      return -area/2;
    }
}
