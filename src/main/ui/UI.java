package main.ui;

import javax.swing.JFrame;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JMenu;
import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JRadioButtonMenuItem;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JSeparator;

import main.utils.AppHelper;
import main.utils.AppHelper.Mode;
import main.utils.AppHelper.Operation;
import javax.swing.JPanel;
import java.awt.Dimension;
import javax.swing.JMenuItem;

public class UI extends JApplet implements MouseListener{

	public JFrame frmDraw;
	JLabel labelMode;
	JPopupMenu popupMenu;
	DrawPanel panelScreen;
	public static JRadioButtonMenuItem rdbtnmntmRed, rdbtnmntmGreen, rdbtnmntmBlue;
	/**
	 * Create the application.
	 */
	public UI() {
		init();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void init() {
		
		// Design of the application
		
//		frmDraw = new JFrame();
		this.getContentPane().setBackground(Color.LIGHT_GRAY);
//		frmDraw.setTitle("Draw");
//		this.setBounds(100, 100, 700, 500);
		resize(700, 500);
//		frmDraw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		popUpMenu();
		addMouseListener(this);
		
		JPanel panelTitle = new JPanel();
		panelTitle.setBounds(0, 0, 700, 24);
		this.getContentPane().add(panelTitle);
		
		labelMode = new JLabel("Mode: Draw | Color: Red | Operation: ");
		panelTitle.add(labelMode);
		
		JSeparator separatorTitle = new JSeparator();
		separatorTitle.setPreferredSize(new Dimension(700, 2));
		panelTitle.add(separatorTitle);
		separatorTitle.setForeground(Color.BLACK);
		
		panelScreen = new DrawPanel();
//		if (AppHelper.currentMode == Mode.Edit)
//		addPopup(panelScreen, popupMenu);
		panelTitle.addMouseListener(this);
		panelScreen.addMouseListener(this);

		panelScreen.setBounds(0, 25, 700, 500);
		this.getContentPane().add(panelScreen);
	}
	
	public void start() {
		this.setVisible(true);
	}
	

	private void popUpMenu() {
		
		this.getContentPane().setLayout(null);
		popupMenu = new JPopupMenu();
		popupMenu.setBounds(-10008, -10031, 107, 72);
//		panelScreen.addMouseListener(this);

		// BEGINING - Creating popup menu with all its element - 
		
		JMenu mnMode = new JMenu("Mode");
		ButtonGroup modeGroup = new ButtonGroup();
		popupMenu.add(mnMode);
		
		JRadioButtonMenuItem rdbtnmntmDraw = new JRadioButtonMenuItem("Draw");
		rdbtnmntmDraw.setSelected(true);
		modeGroup.add(rdbtnmntmDraw);
		mnMode.add(rdbtnmntmDraw);
		
		JRadioButtonMenuItem rdbtnmntmEdit = new JRadioButtonMenuItem("Edit");
		modeGroup.add(rdbtnmntmEdit);
		mnMode.add(rdbtnmntmEdit);
		
		JMenu mnColor = new JMenu("Color");
		ButtonGroup colorGroup = new ButtonGroup();
//		popupMenu.add(mnColor);
		
		rdbtnmntmRed = new JRadioButtonMenuItem("Red");
		rdbtnmntmRed.setSelected(true);
		colorGroup.add(rdbtnmntmRed);
		mnColor.add(rdbtnmntmRed);
		
		rdbtnmntmGreen = new JRadioButtonMenuItem("Green");
		colorGroup.add(rdbtnmntmGreen);
		mnColor.add(rdbtnmntmGreen);
		
		rdbtnmntmBlue = new JRadioButtonMenuItem("Blue");
		colorGroup.add(rdbtnmntmBlue);
		mnColor.add(rdbtnmntmBlue);
		
		JMenu mnOperation = new JMenu("Operation");
		ButtonGroup operationGroup = new ButtonGroup();
//		popupMenu.add(mnOperation);
		
		JRadioButtonMenuItem rdbtnmntmTranslate = new JRadioButtonMenuItem("Translate");
		operationGroup.add(rdbtnmntmTranslate);
		mnOperation.add(rdbtnmntmTranslate);
		
		JRadioButtonMenuItem rdbtnmntmScale = new JRadioButtonMenuItem("Scale");
		operationGroup.add(rdbtnmntmScale);
		mnOperation.add(rdbtnmntmScale);
		
		JMenuItem mntmRotate = new JMenuItem("Rotate");
		mnOperation.add(mntmRotate);
		
		JMenuItem mntmFill = new JMenuItem("Fill");
		mnOperation.add(mntmFill);
		
		JMenuItem mntmDelete = new JMenuItem("Delete");
		mnOperation.add(mntmDelete);
		
		// END - Creating popup menu with all its element - 
		
		// BEGINING - SUB Menu button listeners -
		
		rdbtnmntmDraw.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AppHelper.currentMode = Mode.Draw;
				labelMode.setText("Mode: " + AppHelper.currentMode + " | Color: " + AppHelper.getCurrentColor() + " | Operation: " + AppHelper.currentOperation);
//				removePopup(panelScreen, popupMenu);
				popupMenu.remove(mnColor);
				popupMenu.remove(mnOperation);
			}
		});
		rdbtnmntmEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AppHelper.currentMode = Mode.Edit;
				labelMode.setText("Mode: " + AppHelper.currentMode + " | Color: " + AppHelper.getCurrentColor() + " | Operation: " + AppHelper.currentOperation);
				DrawPanel.clearPoints = true;			
				popupMenu.add(mnColor);
				popupMenu.add(mnOperation);
			}
		});
		rdbtnmntmRed.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AppHelper.currentColor = Color.RED;
				labelMode.setText("Mode: " + AppHelper.currentMode + " | Color: " + AppHelper.getCurrentColor() + " | Operation: " + AppHelper.currentOperation);
				panelScreen.repaint();
			}
		});
		rdbtnmntmGreen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AppHelper.currentColor = Color.GREEN;
				labelMode.setText("Mode: " + AppHelper.currentMode + " | Color: " + AppHelper.getCurrentColor() + " | Operation: " + AppHelper.currentOperation);
				panelScreen.repaint();
			}
		});
		rdbtnmntmBlue.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AppHelper.currentColor = Color.BLUE;
				labelMode.setText("Mode: " + AppHelper.currentMode + " | Color: " + AppHelper.getCurrentColor() + " | Operation: " + AppHelper.currentOperation);
				panelScreen.repaint();
			}
		});
		rdbtnmntmTranslate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AppHelper.currentOperation = Operation.Translate;
				labelMode.setText("Mode: " + AppHelper.currentMode + " | Color: " + AppHelper.getCurrentColor() + " | Operation: " + AppHelper.currentOperation);
				panelScreen.repaint();
			}
		});
		rdbtnmntmScale.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AppHelper.currentOperation = Operation.Scale;
				labelMode.setText("Mode: " + AppHelper.currentMode + " | Color: " + AppHelper.getCurrentColor() + " | Operation: " + AppHelper.currentOperation);
				panelScreen.repaint();
			}
		});
		mntmDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AppHelper.currentOperation = Operation.Delete;
				labelMode.setText("Mode: " + AppHelper.currentMode + " | Color: " + AppHelper.getCurrentColor() + " | Operation: " + AppHelper.currentOperation);
				panelScreen.repaint();
			}
		});
		mntmFill.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AppHelper.currentOperation = Operation.Fill;
				AppHelper.currentColor = AppHelper.getSelectedColorFromMenu();
				labelMode.setText("Mode: " + AppHelper.currentMode + " | Color: " + AppHelper.getCurrentColor() + " | Operation: " + AppHelper.currentOperation);
				DrawPanel.setFillForIntersect();
				panelScreen.repaint();
			}
		});
		mntmRotate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AppHelper.currentOperation = Operation.Rotate;
				labelMode.setText("Mode: " + AppHelper.currentMode + " | Color: " + AppHelper.getCurrentColor() + " | Operation: " + AppHelper.currentOperation);
				panelScreen.repaint();
			}
		});
		/*scale05.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panelScreen.scaleMultipier = 1.1;
				setCurrentMenuState(Operation.Scale);
			}
		});
		scale10.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panelScreen.scaleMultipier = 1;
				setCurrentMenuState(Operation.Scale);
			}
		});
		scale15.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panelScreen.scaleMultipier = 1.5;
				setCurrentMenuState(Operation.Scale);
			}
		});
		scale20.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panelScreen.scaleMultipier = 2;
				setCurrentMenuState(Operation.Scale);
			}
		});
		scale30.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panelScreen.scaleMultipier = 3;
				setCurrentMenuState(Operation.Scale);
			}
		}); */
		// BEGINING - SUB Menu button listeners -
	}

//	private static void addPopup(Component component, final JPopupMenu popup) {
//
//		// Popup listeners and conditions for showing
//		
//		component.addMouseListener(new MouseAdapter() {
//			public void mousePressed(MouseEvent e) {
//				if (e.isPopupTrigger() || (e.getButton() == 1 && e.getY() < 40)) {
//					showMenu(e);
//				}
//			}
//			public void mouseReleased(MouseEvent e) {
//				if (e.isPopupTrigger()) {
//					showMenu(e);
//				}
//			}
//			private void showMenu(MouseEvent e) {
//				if (AppHelper.currentMode == Mode.Edit || DrawPanel.showPopup) {
//					popup.show(e.getComponent(), e.getX(), e.getY());
//					DrawPanel.showPopup = false;
//				}
//			}
//		});
//	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.isPopupTrigger() || (e.getButton() == 1 && e.getY() < 40)) {
			showMenu(e);
		}		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.isPopupTrigger()) {
			showMenu(e);
		}		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	private void showMenu(MouseEvent e) {
		if (AppHelper.currentMode == Mode.Edit || DrawPanel.showPopup) {
			popupMenu.show(e.getComponent(), e.getX(), e.getY());
			DrawPanel.showPopup = false;
		}
	}
}
