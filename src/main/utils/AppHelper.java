package main.utils;

import java.awt.Color;

import main.ui.UI;

public class AppHelper {
	public static Mode currentMode = Mode.Draw;
	public static Color currentColor = Color.RED;
	public static Operation currentOperation;
	
	public enum Mode {
		Draw,
		Edit;
	}
	public enum Operation {
		Translate,
		Scale,
		Rotate,
		Fill,
		Delete;
	}
	
	/**
	 * 
	 * @return a string with the selected color from the Color SubMenu
	 */
	public static String getCurrentColor() {
		switch (currentColor.getRGB()) {
		case -65536: return "Red";
		case -16711936: return "Green";
		case -16776961: return "Blue";
		default: return "unknown";
		}
	}
	
	/**
	 * 
	 * @return the selected color from the Color SubMenu
	 */
	public static Color getSelectedColorFromMenu() {
		if (UI.rdbtnmntmBlue.isSelected()) {
			return Color.BLUE;
		} else if (UI.rdbtnmntmGreen.isSelected()) {
			return Color.GREEN;
		} else {
			return Color.RED;
		}
	}
}
