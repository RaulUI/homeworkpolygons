package main.utils;

import java.awt.Color;
import java.awt.Polygon;

public class MyPoly extends Polygon {

	private Color fillColor;
	private int rotationAngle;
	private double scale;
	
	public MyPoly() {
		super();
	}
	
	public MyPoly(Color fillColor, int rotationAngle, double scale) {
		super();
		this.fillColor = fillColor;
		this.rotationAngle = rotationAngle;
		this.scale = scale;
	}
	public MyPoly(int[] pointsArrayX, int[] pointsArrayY, int size, Color fillColor) {
		this.xpoints = pointsArrayX;
		this.ypoints = pointsArrayY;
		this.npoints = size;
		this.fillColor = fillColor;
		rotationAngle = 0;
		scale = 1;
	}

	public Color getFillColor() {
		return fillColor;
	}
	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
	}
	public int getRotationAngle() {
		return rotationAngle;
	}
	public void setRotationAngle(int rotationAngle) {
		this.rotationAngle = rotationAngle;
	}
	public double getScale() {
		return scale;
	}
	public void setScale(double scale) {
		this.scale = scale;
	}
}
